/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Boby Love
 */
public class Question1 {
    
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> list = new ArrayList<String>();
        
        //form list of numb from 0-9
        for(int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }
        
        //Insert a new set of num from 0-5
        for(int i = 0; i < 5; i++) {
            list.add(String.valueOf(i));
        }
        
        System.out.println("Input list: " + list);
        System.out.println("\nFiltered duplicates: " + processList(list));
    }
    
    public static Set<String> processList(List<String> listContainingDuplicates) {
        
        final Set<String> resultSet = new HashSet<String>();
        final Set<String> tempSet = new HashSet<String>();
        
        for(String yourInt:listContainingDuplicates) {
            if(!tempSet.add(yourInt)) {
                resultSet.add(yourInt);
            }
        }
        
        return resultSet;
    }
    
}
